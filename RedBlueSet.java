package seminar5;


class RedBlueSet {
    int[] redContents;
    int redCount;
    int[] blueContents;
    int blueCount;

    void addRed(int c) {
        redContents[redCount++] = c;
    }

    int indexOfRed(int c) {
        for (int i = 0; i < redCount; i++)
            if (redContents[i] == c) return i;
        return -1;
    }

    boolean containsRed(int c) {
        return indexOfRed(c) != -1;
    }

    void removeRed(int c) {
        int i = indexOfRed(c);
        if (i == -1) return;
        redContents[i] = redContents[--redCount];
    }

    void addBlue(int c) {
        if (containsBlue(c)) return;
        blueContents[blueCount++] = c;
    }

    int indexOfBlue(int c) {
        for (int i = 0; i < blueCount; i++)
            if (blueContents[i] == c) return i;
        return -1;
    }

    boolean containsBlue(int c) {
        return indexOfBlue(c) != -1;
    }

    void removeBlue(int c) {
        int i = indexOfBlue(c);
        if (i == -1) return;
        blueContents[i] = blueContents[--blueCount];
    }
}
