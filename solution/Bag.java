package solution.seminar5;

/**
 * Trida Bag reprezentuje kolekci cisel s opakovanim,
 * dvakrat vlozene cislo je tam dvakrat a je ho potreba dvakrat vyjmout.
 */
public class Bag {
    private int[] contents = new int[1000];
    private int size = 0;

    void add(int e) {
        contents[size++] = e;
    }

    boolean contains(int e) {
        return indexOf(e) != -1;
    }

    int indexOf(int e) {
        for (int i = 0; i < size; i++)
            if (contents[i] == e)
                return i;
        return -1;
    }

    void remove(int e) {
        int i = indexOf(e);
        if (i == -1)
            return;
        contents[i] = contents[--size];
    }
}
