package solution.seminar5;

/**
 * Created by drchajan on 19/03/14.
 */
public class MyStack implements WeirdStack {
    private StackImpl s = new StackImpl();

    @Override
    public void push(int i) {
        s.push(i);
    }

    @Override
    public void push(int[] is) {
        for (int i : is) {
            this.push(i);
        }
    }

    @Override
    public int top() {
        int t = this.pop();
        this.push(t);
        return t;
    }

    @Override
    public int pop() {
        return s.pop();
    }

    @Override
    public int popFirstNegativeElement() {
        StackImpl t = new StackImpl();
        int i = 0;
        while (!s.isEmpty()) {
            i = s.pop();
            if (i < 0) {
                break;
            }
            t.push(i);
        }

        while (!t.isEmpty()) {
            s.push(t.pop());
        }

        if (i >= 0) {
            throw new IllegalStateException("neobsahuje zaporne cislo!");
        }
        return i;
    }

    @Override
    public boolean isEmpty() {
        return s.isEmpty();
    }
}
