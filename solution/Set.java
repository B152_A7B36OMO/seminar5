package solution.seminar5;

/**
 * Trida Set reprezentuje mnozinu cisel, dvakrat vlozene cislo je
 * v ni jen jednou.
 */
class Set {
    Bag b = new Bag();

    void add(int e) {
        if(!b.contains(e)) {
            b.add(e);
        }
    }

    int indexOf(int e) {
        return b.indexOf(e);
    }

    boolean contains(int e) {
        return b.contains(e); // je potreba vymenit za opravdovy kod
    }

    void remove(int e) {
        b.remove(e);
    }
}