package solution.seminar5;


public interface WeirdStack {
    //vlozi na vrchol zasobniku i
    public void push(int i);

    //vlozi postupne na vrchol zasobniku vsechny prvky is
    public void push(int[] is);

    //vrati hodnotu na vrcholu zasobniku (neodstranuje ji)
    public int top();

    //vrati hodnotu na vrcholu zasobniku (odstrani ji)
    public int pop();

    //projde zasobnik od vrcholu a vrati prvni zaporny prvek (ten je ze zasobniku odstranen)
    public int popFirstNegativeElement();

    //testuje, zda je zasobnik prazdny
    public boolean isEmpty();
}
