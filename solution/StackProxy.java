package solution.seminar5;

/**
 * Created by drchajan on 19/03/14.
 */
public class StackProxy implements Stack {
    private StackImpl s = null;

    private void init() {
        if(s == null) {
            s = new StackImpl();
        }
    }

    @Override
    public void push(int i) {
        init();
        s.push(i);
    }

    @Override
    public int pop() {
        init();
        return s.pop();
    }

    @Override
    public boolean isEmpty() {
        init();
        return s.isEmpty();
    }
}
