package solution.seminar5;

/**
 * Created by drchajan on 19/03/14.
 */
public interface Stack {
    public void push(int i);

    public int pop();

    public boolean isEmpty();
}
