package solution.seminar5;


class RedBlueSet {
    private Bag redContents = new Bag();
    private Set blueContents = new Set();

    void addRed(int c) {
        redContents.add(c);
    }

    int indexOfRed(int c) {
        return redContents.indexOf(c);
    }

    boolean containsRed(int c) {
        return redContents.contains(c);
    }

    void removeRed(int c) {
        redContents.remove(c);
    }

    void addBlue(int c) {
        blueContents.add(c);
    }

    int indexOfBlue(int c) {
        return blueContents.indexOf(c);
    }

    boolean containsBlue(int c) {
        return blueContents.contains(c);
    }

    void removeBlue(int c) {
        blueContents.remove(c);
    }
}
