package solution.seminar5;

/**
 * User: macekond
 * Date: 17.03.14
 * Time: 13:03
 */
public class StackImpl implements Stack{
    private int[] contents = new int[10000];
    private int size = 0;

    StackImpl() {
        System.out.println("Stack instanciovan");
    }

    public void push(int i) {
        contents[size++] = i;
    }

    public int pop() {
        return contents[--size];
    }

    public boolean isEmpty() {
        return size == 0;
    }
}
